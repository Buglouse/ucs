#!/bin/bash

export ENVIRONMENTVAR

foo() {
    return 0
}

[[ "${0}" = "${BASH_SOURCE}" ]] || return 0

foo ${@}
